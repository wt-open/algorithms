<?php
	if (empty($args['article_state'])) {
		// echo 'IS EMPTY<br>';
		if ($args['story']['post_type'] == 'projects') {
			$args['article_state'] = 'publish';
		} else {
			$args['article_state'] = 'draft';
		}
	} else {
		// echo 'NOT EMPTY<br>';
		
	}
	// print_r($args);
	// exit;
	$q = "SELECT count(ID) as revision_count 
	FROM `wp_posts` 
	WHERE post_parent = ".$args['story']['ID']." 
		and `post_date_gmt` > '".$this->optvars['date_for_speed']."' 
		AND post_type = 'revision' 
		AND post_name NOT LIKE '%autosave%' ";

	$r = $this->db->rawQuery($q);		
	
	$revisions_per_period = (!empty($r)) ? $r[0]['revision_count'] : 0;

	$q = "SELECT count(comment_ID) as comment_count FROM `wp_comments` 
		WHERE comment_post_ID = ".$args['story']['ID']."
	 	AND comment_date_gmt > '".$this->optvars['date_for_speed']."' 
	 	AND comment_approved = 1";
	$r = $this->db->rawQuery($q);		
	$comments_per_period = (!empty($r)) ? $r[0]['comment_count'] : 0;
	
	$q = "SELECT comment_date 
		FROM `wp_comments` WHERE 
		comment_post_ID = ".$args['story']['ID']." AND 
		comment_approved = 1 
		ORDER by `comment_date_gmt` DESC LIMIT 1";
	$r = $this->db->rawQuery($q);		
	$last_comment_date = (!empty($r)) ? $r[0]['comment_date'] : '';

	
			
	if(($args['story']['post_type'] == 'stories') && ($args['article_state'] == 'publish') && (!empty($accepted))) {
       $q = "SELECT count(ID) as pending_revisions_count 
       		FROM `wp_posts` 
       		WHERE post_parent = ".$args['story']['ID']." 
       			AND `post_date_gmt` > '".$accepted['post_date_gmt']."' 
       			AND ID != ".$accepted['ID']." 
       			AND post_type = 'revision' 
       			AND post_name NOT LIKE '%autosave%'  ";
        $r = $this->db->rawQuery($q);  
       	$pending_revisions_count = $r[0]['pending_revisions_count'];

        
    } else {
        $pending_revisions_count = 0;
    }

   $q = "SELECT count(post_id) as pending_revisions_count FROM `wp_postmeta` WHERE `meta_key` = '_wt_is_pending_revision_for' AND `meta_value` = ".$args['story']['ID'];
    $r = $this->db->rawQuery($q);  
   	$pending_revisions_count_meta = $r[0]['pending_revisions_count'];
   	// echo $pending_revisions_count_meta;
   	// exit;

    // if ($args['story']['ID'] == 87424) {
    // 	print_r($q);
    // 	echo '<p>'.$pending_revisions_count.'</p>'; 
    // 	print_r($accepted);
    // 	exit;
    // }
	// if ($args['story']['ID'] == 93873) {
		// die($args['article_state']."TEST");
	// }
	if ($args['article_state'] == 'publish') {

		// $q = "SELECT p.`ID`, p.`post_date_gmt`, m.`meta_value`
		// FROM `wp_posts` p 
		// LEFT JOIN `wp_postmeta` m on p.`ID` = m.`post_id` and `meta_key` = 'article_state' 
		// WHERE `post_parent` = ".$args['story']['ID']." 
		// 	AND `post_type` = 'revision' 
		// 	AND `meta_value` = 'publish' 
		// 	AND `post_name` NOT LIKE '%autosave%' 
		// ORDER by post_date ASC LIMIT 1";
		$q = "SELECT log_date FROM `wp_wt_logcentral_log` WHERE `log_action_id` = 32 AND `post_id` = ".$args['story']['ID'].' LIMIT 1';

 		$r = $this->db->rawQuery($q);  


		if (!empty($r[0])) {
			// print_r($r);
			// exit;
	  		$newtab_date =  $r[0]['log_date'];
	  	} else {
	  		$newtab_date =  $args['story']['post_date_gmt'];
	  	}
	} else {
		if (strlen($args['story']['post_content']) >= $this->optvars['min_length_of_content_for_new']) {
			$newtab_date = $args['story']['post_date_gmt'];
		} else {
			$newtab_date = "0000-00-00";
		}
		
	}
	
	$talktab_date = $last_comment_date;
	$worktab_date = $args['story']['post_modified_gmt'];
	$attention = [];

	if ($pending_revisions_count > 0) {
		$attention[] = 'has_pending';
	}
	if ($pending_revisions_count_meta > 0) {
		$attention[] = 'has_pending_meta';
	}


	$q = "SELECT * FROM `wp_term_relationships` 
		WHERE `term_taxonomy_id` = '".$this->optvars['term_taxonomy_id']."' 
		AND object_id = ".$args['story']['ID'];
		// echo $q;
		// exit;
	$r = $this->db->rawQuery($q);		
	if (!empty($r)) {
		$attention[] = 'is_factcheck';
	}	
	$weight = $this->getPostMeta($args['story']['ID'], 'hpweight');
	// echo $args['story']['ID'].' '.$weight.'<Br>';
	if ($weight == 'boost') {

		$attention[] = 'hp_boost';

		$date_boosted = $this->getPostMeta($args['story']['ID'], 'date_boosted');
		$newdate = date('Y-m-d H:I:s', (strtotime($date_boosted) + $this->optvars['boost_seconds']));

		$newtab_date = ($newdate > $newtab_date) ? $newdate : $newtab_date;
		$talktab_date = ($newdate > $newtab_date) ? $newdate : $talktab_date;
		$worktab_date = ($newdate > $newtab_date) ? $newdate : $worktab_date;
		// die("boosted");
	} 

	$hide = $this->getPostMeta($args['story']['ID'], 'article_hide_status');
	$extent = $this->getPostMeta($args['story']['ID'], 'article_hide_extent');
	if (empty($hide)) {
		$hide = 'nothidden';
	}
	$attention[] = 'hide_state_'.$hide;
	if (!empty($extent)) {
		$attention[] = 'hide_extent_'.$extent;
	}

	$return = [
		'post_id'					=>	$args['story']['ID'],
		'newtab_date'				=>	$newtab_date,
		'talktab_date'				=>	$talktab_date,
		'worktab_date'				=>	$worktab_date,
		'speed_comments'			=>	$comments_per_period ,
		'edit_speed'				=>	$revisions_per_period,
		'attention'					=>	$attention
	];
	return ($return);

?>