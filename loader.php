<?php 
if (!class_exists('MysqliDb')) {
    require_once   'vendor/autoload.php';
}
if (!class_exists('WT_Algorithms')) {
	class WT_Algorithms {
		private $optvars;
		private $dbo;

		public function __construct($mysql_host, $mysql_user, $mysql_pass, $mysql_db) {
			$this->db = new MysqliDb (
			    $mysql_host,
			    $mysql_user,
			    $mysql_pass,
			    $mysql_db
			);
		}
		// public function setOpt ($optvars) {
		// 	$this->optvars = $optvars;
		// }
		public function getPublishedActionID () {
			$q = "SELECT action_id FROM `wp_wt_logcentral_actions` WHERE `log_label` LIKE 'set_articlestate_publish'";
			$r = $this->db->rawQuery($q);	
			if (!empty($r)) {
				return($r[0]->action_id);
			} else {
				return;
			}

		}
		public function addFactCheckTermOpt() {
			$factcheckterm = [];
			$q = "SELECT term_id FROM `wp_terms` WHERE `slug` = 'fact-check'";
			$r = $this->db->rawQuery($q);		
			$factcheckterm['term_id'] = (!empty($r)) ? $r[0]['term_id'] : '';
			if (!empty($factcheckterm['term_id'])) {
				$q = "SELECT term_taxonomy_id FROM `wp_term_taxonomy` WHERE `term_id` = ".$factcheckterm['term_id'];
				$r = $this->db->rawQuery($q);		
				$factcheckterm['term_taxonomy_id'] = (!empty($r)) ? $r[0]['term_taxonomy_id'] : '';
				// print_r($factcheckterm);
				// exit;
				$this->optvars['term_taxonomy_id'] = $factcheckterm['term_taxonomy_id'];
				
			} else {
				$this->optvars['term_taxonomy_id'] = '';
			}
		}
		private function getOption ($option_name) {
	        $r = $this->db->rawQuery("
	            SELECT  option_value FROM wp_options
	            WHERE option_name = '$option_name' LIMIT 1
	        ");
	        $r = reset($r);
	        if (!empty($r)) {
	            return($r['option_value']);    
	        }
	    }
	    public function getPostMeta($post_id, $meta_name) {
	        $r = $this->db->rawQuery("
	            SELECT * FROM `wp_postmeta` WHERE `post_id` = '$post_id' AND `meta_key` LIKE '$meta_name' LIMIT 1
	        ");
	        $r = reset($r);
	        if (!empty($r)) {
	            return($r['meta_value']);    
	        }
	    }
		public function setOptVars ($what) {
			switch($what) {
				case 'algaerythm':
					$options = [
						'speedper' => $this->getOption('datachomped_homepagelist_algaerythm_speedtime'),
						'boosttime' => $this->getOption('datachomped_homepagelist_algaerythm_boosttime'),
						'mincharsfornew' => $this->getOption('datachomped_homepagelist_algaerythm_mincharsfornew')
					];

					$this->optvars = [
						'date_for_speed'		=> date('Y-m-d H:i:s', time() - $options['speedper']),
						'boost_seconds'			=> $options['boosttime'],
						'min_length_of_content_for_new'	=> $options['mincharsfornew'],
					];
					$this->addFactCheckTermOpt();

				break;
			}
		}
		public function run ($what, $args) {
			switch($what) {
				case 'algaerythm':
					include("algae-rythm.php");
				break;
			}
			return ($return);
		}
	}
}
?>